<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BaseDialog</name>
    <message>
        <source>Disk test</source>
        <translation>ཁབ་ལེན་གྱི་ཚོད་ལྟ།</translation>
    </message>
</context>
<context>
    <name>DeviceOperation</name>
    <message>
        <source>unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>FDClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>ཤོང་ཚད་སྟོང་པ།</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>སྟོང་བའི་འོད་སྡེར།</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>སྤྱོད་མཁན་གཞན་དག་གི་སྒྲིག་</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Eject</source>
        <translation type="vanished">འཕར་ཐོན།</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>མ་རྩ་ཅན་དུ་འགྱུར་བར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>eject</source>
        <translation type="unfinished">ཕྱིར་འབུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>FDFrame</name>
    <message>
        <source>eject</source>
        <translation>ཕྱིར་འབུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>FormateDialog</name>
    <message>
        <source>Formatted successfully!</source>
        <translation>རྣམ་གཞག་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Formatting failed, please unplug the U disk and try again!</source>
        <translation>རྣམ་གཞག་ལ་ཕམ་ཉེས་བྱུང་བས་ཁྱེད་ཀྱིས་ཝུའུ་ཁི་ལན་གྱི་ཁབ་ལེན་སྡེར་མ་བླངས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Rom size:</source>
        <translation>Rom ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <source>Filesystem:</source>
        <translation>ཡིག་ཚགས་མ་ལག་ནི།</translation>
    </message>
    <message>
        <source>Disk name:</source>
        <translation>ཁབ་ལེན་གྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>རྦད་དེ་མེད་པར་བཟོ་དགོས། (དུས་ཚོད་ཅུང་རིང་བས་ཁྱེད་ཀྱིས་གཏན་འཁེལ་གནང་རོགས། )</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation type="vanished">རྣམ་གཞག་གི་ཁབ་ལེན་སྡེར་མ</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please back up all retained data before formatting. Do you want to continue?</source>
        <translation>ཤོག་དྲིལ་འདི་རྣམ་བཞག་ཅན་བྱས་ན་སྟེང་གི་གཞི་གྲངས་ཐམས་ཅད་མེད་པར་འགྱུར། ཁྱོད་ཀྱིས་རྣམ་བཞག་ཅན་དུ་བསྒྱུར་བའི་སྔོན་ལ་གཞི་གྲངས་གྲབས་གསོག་བྱོས། མུ་མཐུད་ཡིན་ནམ།？</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk format</source>
        <translation>ཁབ་ལེན་སྡེར་མའི་རྣམ་གཞག</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>usb management tool</source>
        <translation>usb དོ་དམ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>ukui-flash-disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kindly reminder</source>
        <translation>བྱམས་བརྩེའི་ངང་དྲན་སྐུལ་བྱེད་</translation>
    </message>
    <message>
        <source>wrong reminder</source>
        <translation>ནོར་འཁྲུལ་གྱི་དྲན་སྐུལ</translation>
    </message>
    <message>
        <source>Please do not pull out the USB flash disk when reading or writing</source>
        <translation>དཔེ་ཆ་ཀློག་པ་དང་རྩོམ་ཡིག་འབྲི་སྐབས་ཨ་སྒོར་གྱི་འོད་ཆེམ་ཆེམ་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <source>Please do not pull out the CDROM when reading or writing</source>
        <translation>ཁྱོད་ཀྱིས་ཡི་གེ་ཀློག་པ་དང་རྩོམ་ཡིག་འབྲི་སྐབས་CDROMཕྱིར་འཐེན་མ་བྱེད།</translation>
    </message>
    <message>
        <source>Please do not pull out the SD Card when reading or writing</source>
        <translation>ཡི་གེ་ཀློག་པ་དང་རྩོམ་ཡིག་འབྲི་སྐབས་SD Cardམ་འཐེན་རོགས།</translation>
    </message>
    <message>
        <source>There is a problem with this device</source>
        <translation type="obsolete">此设备存在问题</translation>
    </message>
    <message>
        <source>telephone device</source>
        <translation>ཁ་པར་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Removable storage device removed</source>
        <translation type="vanished">移动存储设备已移除</translation>
    </message>
    <message>
        <source>Please do not pull out the storage device when reading or writing</source>
        <translation>དཔེ་ཆ་ཀློག་པ་དང་རྩོམ་ཡིག་འབྲི་སྐབས་གསོག་ཉར་སྒྲིག་ཆས་ཕྱིར་འཐེན་མ་བྱེད།</translation>
    </message>
    <message>
        <source>Storage device removed</source>
        <translation>གསོག་ཉར་སྒྲིག་ཆས་མེད་པར་བཟོ་</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <source>ukui flash disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kylin device daemon</source>
        <translation>ཅིན་ལིན་སྒྲིག་ཆས་ཀྱི་ཏའེ་མོན་</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
</context>
<context>
    <name>QClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>ཤོང་ཚད་སྟོང་པ།</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>སྟོང་བའི་འོད་སྡེར།</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>སྤྱོད་མཁན་གཞན་དག་གི་སྒྲིག་</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>མ་རྩ་ཅན་དུ་འགྱུར་བར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>弹出</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RepairDialogBox</name>
    <message>
        <source>Disk test</source>
        <translation>ཁབ་ལེན་གྱི་ཚོད་ལྟ།</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk and drive are properly connected, make sure the disk is not a read-only disk, and try again. For more information, search for help on read-only files and how to change read-only files.&lt;/p&gt;</source>
        <translation type="vanished">&lt;h4&gt;系统无法识别U盘内容&lt;/h4&gt;&lt;p&gt;检查磁盘和驱动器是否正确连接，确保磁盘不是只读磁盘，然后重试。有关更多信息，请搜索有关只读文件和如何更改只读文件的帮助。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation>རྣམ་གཞག་གི་ཁབ་ལེན་སྡེར་མ</translation>
    </message>
    <message>
        <source>Repair</source>
        <translation>ཞིག་གསོ་</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt; 系统&lt;/h4&gt;&lt;p&gt;&lt;p&gt;ཞིབ་བཤེར་བྱས་ནས་ཁབ་ལེན་འཁོར་ལོ་དང་སྒུལ་ཤུགས་འབྲེལ་མཐུད་འོས་འཚམ་བྱས་ཏེ་ཁབ་ལེན་གྱི་ཁབ་ལེན་དེ་ཀློག་པ་པོ་ཁོ་ནའི་ཁབ་ལེན་མ་ཡིན་པར་ཁག་ཐེག་བྱས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱས། ཆ་འཕྲིན་སྔར་ལས་མང་བ་ཞིག་ཀློག་པ་ཁོ་ནའི་ཡིག་ཆའི་སྟེང་ནས་རོགས་རམ་འཚོལ་ཞིབ་བྱས་ཏེ་ཀློག་འདོན་ཁོ་ནའི་ཡིག་ཆ་བསྒྱུར་བཅོས་བྱེད་དགོས། &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive &apos;%1&apos; is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt; 系统&lt;/h4&gt;&lt;p&gt;&lt;p&gt;ཞིབ་བཤེར་བྱས་ནས་ཁབ་ལེན་ཁབ་ལེན་གྱི་&apos;%1&apos;འབྲེལ་མཐུད་འོས་འཚམ་བྱས་ཏེ་ཁབ་ལེན་ཁབ་ལེན་གྱི་ཁབ་ལེན་མ་ཡིན་པར་ཁག་ཐེག་བྱས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་དགོས། ཆ་འཕྲིན་སྔར་ལས་མང་བ་ཞིག་ཀློག་པ་ཁོ་ནའི་ཡིག་ཆའི་སྟེང་ནས་རོགས་རམ་འཚོལ་ཞིབ་བྱས་ཏེ་ཀློག་འདོན་ཁོ་ནའི་ཡིག་ཆ་བསྒྱུར་བཅོས་བྱེད་དགོས། &lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>RepairProgressBar</name>
    <message>
        <source>&lt;h3&gt;%1&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;%1&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Attempting a disk repair...</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་ཞིག་གསོ་བྱེད་རྩིས་ཡོད།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Repair successfully!</source>
        <translation>ཞིག་གསོ་ལེགས་འགྲུབ་བྱུང་བ་རེད།！</translation>
    </message>
    <message>
        <source>The repair completed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation type="vanished">修复失败，如果设备没有成功挂载，请尝试格式化修复！</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk repair</source>
        <translation>ཁབ་ལེན་གྱི་ཞིག་གསོ།</translation>
    </message>
    <message>
        <source>Repair failed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation>ཞིག་གསོ་བྱེད་མ་ཐུབ་པ་རེད། གལ་ཏེ་ཨ་སྒོར་གྱི་གློག་འཁྱུག་སྡེར་མ་སྒྲིག་སྦྱོར་མ་བྱས་ན། སྒྲིག་ཆས་འདི་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།！</translation>
    </message>
</context>
<context>
    <name>ejectInterface</name>
    <message>
        <source>usb has been unplugged safely</source>
        <translation type="vanished">U盘已安全拔出</translation>
    </message>
    <message>
        <source>cdrom has been unplugged safely</source>
        <translation type="vanished">光盘已安全拔出</translation>
    </message>
    <message>
        <source>sd has been unplugged safely</source>
        <translation type="vanished">SD卡已安全拔出</translation>
    </message>
    <message>
        <source>usb is occupying unejectable</source>
        <translation type="vanished">U盘占用无法弹出</translation>
    </message>
    <message>
        <source>Storage device can be safely unplugged</source>
        <translation>གསོག་ཉར་སྒྲིག་ཆས་བདེ་འཇགས་ངང་མེད་པར་བཟོ་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>gpartedInterface</name>
    <message>
        <source>ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <source>gparted has started,can not eject</source>
        <translation>མགོ་བརྩམས་ཟིན་པས་ཕྱིར་འབུད་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>interactiveDialog</name>
    <message>
        <source>usb is occupying,do you want to eject it</source>
        <translation>usbབཙན་བཟུང་བྱེད་བཞིན་ཡོད། ཁྱོད་ཀྱིས་དེ་ཕྱིར་འབུད་བྱེད་འདོད་ཡོད་མེད།</translation>
    </message>
    <message>
        <source>cdrom is occupying,do you want to eject it</source>
        <translation>cdromབཙན་བཟུང་བྱེད་བཞིན་ཡོད། ཁྱོད་ཀྱིས་དེ་ཕྱིར་འབུད་བྱེད་འདོད་ཡོད་མེད།</translation>
    </message>
    <message>
        <source>sd is occupying,do you want to eject it</source>
        <translation>sdནི་བཙན་བཟུང་བྱས་པ་ཡིན་པས། ཁྱོད་ཀྱིས་དེ་ཕྱིར་འབུད་བྱེད་འདོད་ཡོད་མེད།</translation>
    </message>
    <message>
        <source>cancle</source>
        <translation>སྒྲོན་མེ།</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ཡིན།</translation>
    </message>
    <message>
        <source>cdrom is occupying</source>
        <translation>cdromནི་བཙན་བཟུང་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <source>sd is occupying</source>
        <translation>sdནི་བཙན་བཟུང་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <source>usb is occupying</source>
        <translation>usbབདག་བཟུང་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
</TS>
